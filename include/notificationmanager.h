#ifndef NOTIFICATIONMANAGER_H
#define NOTIFICATIONMANAGER_H

#include <QDBusAbstractAdaptor>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusMessage>
#include <QObject>
#include <QQmlApplicationEngine>
#include <QQmlComponent>

class NotificationManager : public QDBusAbstractAdaptor
{
  Q_OBJECT
  Q_CLASSINFO("D-Bus Interface", "org.freedesktop.Notifications")

public:
  NotificationManager(QQmlApplicationEngine* m_engine,
                      const QString& path,
                      QObject* parent = nullptr);
  virtual ~NotificationManager();

private slots:
  void triggerNext();

public slots:
  Q_NOREPLY void Notify(const QDBusMessage& msg);
  QString GetServerInformation(QString& vendor,
                               QString& version,
                               QString& spec_version);

private:
  QQmlApplicationEngine* m_engine;
  QList<QVariantMap> m_queue;

  // Currently visible notification
  QObject* m_currentObject;
  QQmlComponent* m_component;

  QVariantMap parseMessage(const QDBusMessage&);
};

#endif // NOTIFICATIONMANAGER_H
